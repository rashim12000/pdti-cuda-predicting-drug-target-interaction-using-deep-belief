package activation;

import org.jblas.FloatMatrix;

/**
 * Created by rashim on 2/23/17.
 */
public interface ActivationFunction<R> {
    /**
     * Applies the derivative of this function
     * @param input the input to apply it to
     * @return the derivative of this function applied to
     * the input
     */
    R apply(FloatMatrix input);


}
