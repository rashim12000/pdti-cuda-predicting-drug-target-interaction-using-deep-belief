package com.waytofreedom;

import org.jblas.FloatMatrix;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Random;

import static com.waytofreedom.JCUDAMatrixUtils.multiply;
import static com.waytofreedom.MatrixUtil.*;

/**
 * Created by rashim on 2/19/17.
 */
public class RBMCudaOne {
    public int N;
    public int n_visible;
    public int n_hidden;
    public FloatMatrix W;
    public FloatMatrix hbias;
    public FloatMatrix vbias;
    public Random rng;


    public RBMCudaOne(int N, int n_visible, int n_hidden,
                      FloatMatrix W, FloatMatrix hbias, FloatMatrix vbias, Random rng) {
        this.N = N;
        this.n_visible = n_visible;
        this.n_hidden = n_hidden;

//        System.out.print("value of n= "+N);
//        System.out.print("value of n_visible= "+n_visible);
//        System.out.print("value of n_hidden= "+n_hidden);

        if (rng == null) this.rng = new Random(1234);
        else this.rng = rng;

        if (W == null) {
//            this.W = new double[this.n_hidden][this.n_visible];
            float[][] Wtemp = new float[this.n_visible][this.n_hidden];


            double a = 1.0 / this.n_visible;
//            System.out.println("**** wight matrix 3*6 in array");
            for (int i = 0; i < this.n_visible; i++) {
                for (int j = 0; j < this.n_hidden; j++) {
                    Wtemp[i][j] = uniform(-a, a, rng);
//                    System.out.print("  "+ Wtemp[i][j]);
                }
//                System.out.println();
            }


            this.W = new FloatMatrix(Wtemp);

//            System.out.println("**** wight matrix  ******");
//            float[][] tmpmatrix = this.W.dup().toArray2();
//            for (int y = 0; y < tmpmatrix.length; y++) {
//                for (int x = 0; x < tmpmatrix[y].length; x++) {
//                    System.out.print("  "+tmpmatrix[y][x]);
//
//                }
//                System.out.println();
//            }
        } else {
            this.W = W;
//            System.out.println("**** wight matrix ******");
//            float[][] tmpmatrix = this.W.dup().toArray2();
//            for (int y = 0; y < tmpmatrix.length; y++) {
//                for (int x = 0; x < tmpmatrix[y].length; x++) {
//                    System.out.print("  "+tmpmatrix[y][x]);
//
//                }
//                System.out.println();
//            }
        }


        if (hbias == null) {
//            this.hbias = new double[this.n_hidden];
            float[] hbiastemp = new float[this.n_hidden];
            for (int i = 0; i < this.n_hidden; i++)
                hbiastemp[i] = (float) 0;
            this.hbias = new FloatMatrix(hbiastemp);
//            print_matrix(this.hbias,"hbias matrix");
        } else {
            this.hbias = hbias;
        }

        if (vbias == null) {
            float[] vbiastemp = new float[this.n_visible];
            for (int i = 0; i < this.n_visible; i++) vbiastemp[i] = (float) 0;
            this.vbias = new FloatMatrix(vbiastemp);

//            print_matrix(this.vbias,"visible vbias matrix ");

        } else {
            this.vbias = vbias;
        }
    }


    public void contrastive_divergence(FloatMatrix input, double lr, int k) {
        Pair<FloatMatrix, FloatMatrix> probHidden = sample_h_given_v(input);
        FloatMatrix chainStart = probHidden.getSecond();
//        FloatMatrix ph_mean_first = probHidden.getFirst();

        Pair<Pair<FloatMatrix, FloatMatrix>, Pair<FloatMatrix, FloatMatrix>> matrices = null;

        //negative visible means or expected values
        FloatMatrix nv_means = null;
        //negative value samples
        FloatMatrix nv_samples = null;
        //negative hidden means or expected values
        FloatMatrix nh_means = null;
        //negative hidden samples
        FloatMatrix nh_samples = null;


        for (int step = 0; step < k; step++) {
            if (step == 0) {
                matrices = gibbs_hvh(chainStart);
            } else {
                matrices = gibbs_hvh(nh_samples);
            }

//            nv_means = matrices.getFirst().getFirst();
//            nv_samples = matrices.getFirst().getSecond();
//            //hidden mean and sample
//            nh_means = matrices.getSecond().getFirst();
//            nh_samples = matrices.getSecond().getSecond();


            //get the cost updates for sampling in the chain after k iterations
            nv_means = matrices.getFirst().getFirst();
            nv_samples = matrices.getFirst().getSecond();
            nh_means = matrices.getSecond().getFirst();
            nh_samples = matrices.getSecond().getSecond();
//            print_matrix(nh_samples, "nh sample");
        }

//        print_matrix(input,"the data to be multiplied");
//        print_matrix(probHidden.getSecond(),"to be multiplied");
//        print_matrix(nv_samples,"the data to be multiplied nv_samples");
//        print_matrix(nh_samples,"the data to be multiplied nh_samples");
//        print_matrix(nh_means,"to be multiplied nh_means");
//        print_matrix(nv_means,"to be multiplied nv_means");
//        print_matrix(ph_mean_first,"to be multiplied ph_means_first");


//        FloatMatrix wGradient = ((data.mmul(probHidden.getSecond()).sub(
//                nv_samples.mmul(nh_means)).div(N)).mmul((float) lr)
//        );


//        FloatMatrix neweight = ((input.transpose().mmul(probHidden.getSecond()).sub(
//                nv_samples.transpose().mmul(nh_means)).div(N)).muli((float) lr)
//        );

        FloatMatrix neweight = ((multiply(input, probHidden.getSecond(), true, false).sub(multiply(
                nv_samples, nh_means, true, false)).div(N)).muli((float) lr)
        );

        W.addi(neweight);
//        System.out.println("final weight ok");
//        float[][] tmpmatrix = W.dup().toArray2();
//        for (int y = 0; y < tmpmatrix.length; y++) {
//            for (int x = 0; x < tmpmatrix[y].length; x++) {
//                System.out.print("  " + tmpmatrix[y][x]);
//
//            }
//            System.out.println();
//        }

//        print_matrix(W, "final weight");

        FloatMatrix hbiastemp = mean(((probHidden.getSecond().sub(nh_means)).div(N)).mul((float) lr), 0);

//        print_matrix(hbiastemp.transpose(),"temporary hbias matrix");

        hbias.addi(hbiastemp);
//        print_matrix(hbias, "final bias");

//
//        float[][] htmpmatrix = hbiastemp.transpose().dup().toArray2();
//        float[] hnewtmpmatrix = new float[n_hidden];
//
//        for (int y = 0; y < htmpmatrix.length; y++) {
////            tmpmatrix[y][0]= 0;
//            for (int x = 0; x < htmpmatrix[y].length; x++) {
//                hnewtmpmatrix[y] +=htmpmatrix[y][x];
//            }
//
//        }
//        hbias = new FloatMatrix(hnewtmpmatrix);
//        print_matrix(hbias,"sum of temporary value of hbias is");


        FloatMatrix vbiastemp = mean(((input.sub(nv_samples)).div(N)).mul((float) lr), 0);

//        print_matrix(vbiastemp,"temporary vbias matrix");
//        float[][] tmpmatrix = vbiastemp.dup().toArray2();
//        float[] newtmpmatrix = new float[n_visible];
//
//        for (int y = 0; y < tmpmatrix.length; y++) {
////            tmpmatrix[y][0]= 0;
//            for (int x = 0; x < tmpmatrix[y].length; x++) {
//                newtmpmatrix[y] +=tmpmatrix[y][x];
//            }
//
//        }
//        vbias = new FloatMatrix(newtmpmatrix);
        vbias.addi(vbiastemp);
//        print_matrix(vbias,"value of vbias is ");
    }

    public Pair<Pair<FloatMatrix, FloatMatrix>, Pair<FloatMatrix, FloatMatrix>> gibbs_hvh(FloatMatrix h) {
        Pair<FloatMatrix, FloatMatrix> vMeanAndSample = sample_v_given_h(h);
        FloatMatrix vSample = vMeanAndSample.getSecond();
        Pair<FloatMatrix, FloatMatrix> hMeanAndSample = sample_h_given_v(vSample);
        return new Pair<>(vMeanAndSample, hMeanAndSample);
    }


    public Pair<FloatMatrix, FloatMatrix> sample_h_given_v(FloatMatrix v) {
        FloatMatrix hmean = propUp(v);
        FloatMatrix hSample = binomial(hmean, 1, rng);

//       print_matrix(hmean, "hmeaneokk");
//        print_matrix(hSample, "hsampleokk");
        return new Pair<FloatMatrix, FloatMatrix>(hmean, hSample);
    }

    public Pair<FloatMatrix, FloatMatrix> sample_v_given_h(FloatMatrix h) {
        FloatMatrix vmean = propDown(h);
        FloatMatrix vSample = binomial(vmean, 1, rng);

//        print_matrix(vmean, "vmean");
//        print_matrix(vSample, "vsample");

        return new Pair<FloatMatrix, FloatMatrix>(vmean, vSample);
    }

    public FloatMatrix propDown(FloatMatrix h) {

//        print_matrix(W,"weight transpose propdown");
//        FloatMatrix preSig = h.mmul(W.transpose()).addRowVector(vbias);
       FloatMatrix preSig= null;
         preSig = multiply(h, W, false, true).addRowVector(vbias);
        return sigmoid(preSig);
    }

    public FloatMatrix propUp(FloatMatrix v) {

//        print_matrix(W," weight before transpoe propup");
//        print_matrix(W.transpose(),"weight after transpoe propup");
//        FloatMatrix preSig = v.mmul(W).addiRowVector(hbias);
        FloatMatrix preSig= null;
         preSig = multiply(v, W, false, false).addiRowVector(hbias);
//       print_matrix(preSig,"presig printed");
        return sigmoid(preSig);

    }
//    public void reconstruct(FloatMatrix v, FloatMatrix reconstructed_v) {
//        float[] htmp = new float[n_hidden];
//        FloatMatrix h = new FloatMatrix(htmp);
//
//        h = propUp(v);
//        FloatMatrix preSig = W.mmul(h);
//
//        reconstructed_v = sigmoid(preSig);
//
//    }

    public FloatMatrix reconstruct(FloatMatrix v) {

        return propDown(propUp(v));
    }


    public static void test_rbm() throws IOException {
        Random rng = new Random(123);

        float learning_rate = (float) 0.1;
        int training_epochs = 10000;
        int k = 1;

        int train_N = 6;
        int test_N = 2;
//        int n_visible = 785;
        int n_visible = 6;
//        int n_hidden = 10;
        int n_hidden = 3;

        // training data
        float[][] train_Xtmp = {
                {1, 1, 1, 0, 0, 0},
                {1, 0, 1, 0, 0, 0},
                {1, 1, 1, 0, 0, 0},
                {0, 0, 1, 1, 1, 0},
                {0, 0, 1, 0, 1, 0},
                {0, 0, 1, 1, 1, 0}
        };
//

        // data loading
//        System.out.println("Data Loading.................");
//        float [][] train_Xtmp=new float[60000][785];
//        ReadCSV r1=new ReadCSV();
//        r1.read_X(train_Xtmp);
//

//        float [][] train_X = new float[100][785];
//        for(int i=0;i<100;i++)
//        {
//            for(int j=0;j<785;j++)
//            {
//                train_X[i][j]=train_Xtmp[i][j];
//            }
//        }


        FloatMatrix train_X = new FloatMatrix(train_Xtmp);
        RBMCudaOne rbm = new RBMCudaOne(train_N, n_visible, n_hidden, null, null, null, rng);
        for (int epoch = 0; epoch < training_epochs; epoch++) {
            rbm.contrastive_divergence(train_X, learning_rate, k);
        }


        float[][] test_Xtmp = {
                {1, 1, 0, 0, 0, 0},
                {0, 0, 0, 1, 1, 0}
        };

//        float [][]test_Xtmp=new float[10][785];
//        for(int i=0;i<10;i++)
//        {
//            for (int j=0;j<785;j++)
//            {
//                test_Xtmp[i][j]=train_X[i][j];
//            }
//        }
//
        FloatMatrix test_X = new FloatMatrix(test_Xtmp);

        FloatMatrix test_final = rbm.reconstruct(test_X);

        System.out.println("**** printing matrix the final matrix");
        float[][] tmpmatrix = test_final.dup().toArray2();
        for (int y = 0; y < tmpmatrix.length; y++) {
            for (int x = 0; x < tmpmatrix[y].length; x++) {
                System.out.print("  " + tmpmatrix[y][x]);
            }
            System.out.println();
        }
//        WriteToCSV wc = new WriteToCSV();
//        wc.write(tmpmatrix);

    }


    public void print_matrix(FloatMatrix tmp, String txt) {

        System.out.println("**** printing matrix *****" + txt);
        float[][] tmpmatrix = tmp.dup().toArray2();
        for (int y = 0; y < tmpmatrix.length; y++) {
            for (int x = 0; x < tmpmatrix[y].length; x++) {
                System.out.print("  " + tmpmatrix[y][x]);

            }
            System.out.println();
        }

    }

    //    public static FloatMatrix transposeMatrix(FloatMatrix tmp){
//
//        float[][] tmpmatrix = tmp.dup().toArray2();
//        float[][] temp = new float[tmpmatrix[0].length][tmpmatrix.length];
////        for (int y = 0; y < tmpmatrix.length; y++) {
////            for (int x = 0; x < tmpmatrix[y].length; x++) {
////                tmpmatrix[y][x]=tmpmatrix[x][y];
////
////            }
////        }
//        for (int i = 0; i < tmpmatrix.length; i++) {
//            for (int j = 0; j < tmpmatrix[0].length; j++) {
//                temp[j][i] = tmpmatrix[i][j];
//            }
//        }
//
//        return new FloatMatrix(temp);
//
//    }
    public static void main(String[] args) throws IOException {

        test_rbm();
    }
}
