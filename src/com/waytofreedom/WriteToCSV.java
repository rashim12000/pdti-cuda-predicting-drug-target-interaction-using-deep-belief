package com.waytofreedom;

import org.jblas.FloatMatrix;

import java.awt.List;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;

public class WriteToCSV {

    public void write(FloatMatrix test_Ytmp) throws IOException {
        // TODO Auto-generated method stub
        //Delimiter used in CSV file
        final String COMMA_DELIMITER = ",";
        final String NEW_LINE_SEPARATOR = "\n";
        //CSV file header

        //   final String FILE_HEADER = "protein1,protein2,protein3,protein4";


//				        String fileName =new String("/home/om/Music/data/predic_DTI.csv");
        String fileName = new String("D:/drug/opm/predict/resultcudaclg.csv");

        FileWriter fileWriter = null;
        fileWriter = new FileWriter(fileName);

        //Write the CSV file header
        //  fileWriter.append(FILE_HEADER.toString());


        //Add a new line separator after the header
        float[][] test_Y = test_Ytmp.dup().toArray2();
        // fileWriter.append(NEW_LINE_SEPARATOR);
        for (int i = 0; i < test_Y.length; i++) {
            for (int j = 0; j < test_Y[i].length; j++) {
                fileWriter.write(String.valueOf((test_Y[i][j])));
                fileWriter.append(COMMA_DELIMITER);
            }

            fileWriter.append(NEW_LINE_SEPARATOR);
        }

        fileWriter.flush();

        fileWriter.close();
    }

    public void write_weight(FloatMatrix test_Ytmp, int layer) throws IOException {
        // TODO Auto-generated method stub
        //Delimiter used in CSV file
        final String COMMA_DELIMITER = ",";
        final String NEW_LINE_SEPARATOR = "\n";
        //CSV file header

        //   final String FILE_HEADER = "protein1,protein2,protein3,protein4";


//				        String fileName =new String("/home/om/Music/data/predic_DTI.csv");
        String fileName = new String("D:/drug/opm/predict/resultweight" + layer + ".csv");

        FileWriter fileWriter = null;
        fileWriter = new FileWriter(fileName);

        //Write the CSV file header
        //  fileWriter.append(FILE_HEADER.toString());


        //Add a new line separator after the header
        float[][] test_Y = test_Ytmp.dup().toArray2();
        // fileWriter.append(NEW_LINE_SEPARATOR);
        for (int i = 0; i < test_Y.length; i++) {
            for (int j = 0; j < test_Y[i].length; j++) {
                fileWriter.write(String.valueOf((test_Y[i][j])));
                fileWriter.append(COMMA_DELIMITER);
            }

            fileWriter.append(NEW_LINE_SEPARATOR);
        }

        fileWriter.flush();

        fileWriter.close();
    }

    public void write_bias(FloatMatrix test_Ytmp, int layer) throws IOException {
        // TODO Auto-generated method stub
        //Delimiter used in CSV file
        final String COMMA_DELIMITER = ",";
        final String NEW_LINE_SEPARATOR = "\n";
        //CSV file header

        //   final String FILE_HEADER = "protein1,protein2,protein3,protein4";


//				        String fileName =new String("/home/om/Music/data/predic_DTI.csv");
        String fileName = new String("D:/drug/opm/predict/resultbias" + layer + ".csv");

        FileWriter fileWriter = null;
        fileWriter = new FileWriter(fileName);

        //Write the CSV file header
        //  fileWriter.append(FILE_HEADER.toString());


        //Add a new line separator after the header
        float[][] test_Y = test_Ytmp.dup().toArray2();
        // fileWriter.append(NEW_LINE_SEPARATOR);
        for (int i = 0; i < test_Y.length; i++) {
            for (int j = 0; j < test_Y[i].length; j++) {
                fileWriter.write(String.valueOf((test_Y[i][j])));
                fileWriter.append(COMMA_DELIMITER);
            }

            fileWriter.append(NEW_LINE_SEPARATOR);
        }

        fileWriter.flush();

        fileWriter.close();
    }

    public void write_loglayer(FloatMatrix test_Ytmp) throws IOException {
        // TODO Auto-generated method stub
        //Delimiter used in CSV file
        final String COMMA_DELIMITER = ",";
        final String NEW_LINE_SEPARATOR = "\n";
        //CSV file header

        //   final String FILE_HEADER = "protein1,protein2,protein3,protein4";


//				        String fileName =new String("/home/om/Music/data/predic_DTI.csv");
        String fileName = new String("D:/drug/opm/predict/resultloglayer.csv");

        FileWriter fileWriter = null;
        fileWriter = new FileWriter(fileName);

        //Write the CSV file header
        //  fileWriter.append(FILE_HEADER.toString());


        //Add a new line separator after the header
        float[][] test_Y = test_Ytmp.dup().toArray2();
        // fileWriter.append(NEW_LINE_SEPARATOR);
        for (int i = 0; i < test_Y.length; i++) {
            for (int j = 0; j < test_Y[i].length; j++) {
                fileWriter.write(String.valueOf((test_Y[i][j])));
                fileWriter.append(COMMA_DELIMITER);
            }

            fileWriter.append(NEW_LINE_SEPARATOR);
        }

        fileWriter.flush();

        fileWriter.close();
    }

    public void write_loglayerbias(FloatMatrix test_Ytmp) throws IOException {
        // TODO Auto-generated method stub
        //Delimiter used in CSV file
        final String COMMA_DELIMITER = ",";
        final String NEW_LINE_SEPARATOR = "\n";
        //CSV file header

        //   final String FILE_HEADER = "protein1,protein2,protein3,protein4";


//				        String fileName =new String("/home/om/Music/data/predic_DTI.csv");
        String fileName = new String("D:/drug/opm/predict/resultloglayerbias.csv");

        FileWriter fileWriter = null;
        fileWriter = new FileWriter(fileName);

        //Write the CSV file header
        //  fileWriter.append(FILE_HEADER.toString());


        //Add a new line separator after the header
        float[][] test_Y = test_Ytmp.dup().toArray2();
        // fileWriter.append(NEW_LINE_SEPARATOR);
        for (int i = 0; i < test_Y.length; i++) {
            for (int j = 0; j < test_Y[i].length; j++) {
                fileWriter.write(String.valueOf((test_Y[i][j])));
                fileWriter.append(COMMA_DELIMITER);
            }

            fileWriter.append(NEW_LINE_SEPARATOR);
        }

        fileWriter.flush();

        fileWriter.close();
    }

    public void write_drug_predict(FloatMatrix test_Ytmp) throws IOException {
        // TODO Auto-generated method stub
        //Delimiter used in CSV file
        final String COMMA_DELIMITER = ",";
        final String NEW_LINE_SEPARATOR = "\n";
        //CSV file header

        //   final String FILE_HEADER = "protein1,protein2,protein3,protein4";


//				        String fileName =new String("/home/om/Music/data/predic_DTI.csv");
        String fileName = new String("D:/drug/opm/predict/drug_structure_predict.csv");

        FileWriter fileWriter = null;
        fileWriter = new FileWriter(fileName);

        //Write the CSV file header
        //  fileWriter.append(FILE_HEADER.toString());


        //Add a new line separator after the header
        float[][] test_Y = test_Ytmp.dup().toArray2();
        // fileWriter.append(NEW_LINE_SEPARATOR);
        for (int i = 0; i < test_Y.length; i++) {
            for (int j = 0; j < test_Y[i].length; j++) {
                fileWriter.write(String.valueOf((test_Y[i][j])));
                fileWriter.append(COMMA_DELIMITER);
            }

            fileWriter.append(NEW_LINE_SEPARATOR);
        }

        fileWriter.flush();

        fileWriter.close();
    }

    public void writeread(float[][] test_Y) throws IOException {
        // TODO Auto-generated method stub
        //Delimiter used in CSV file
        final String COMMA_DELIMITER = ",";
        final String NEW_LINE_SEPARATOR = "\n";
        //CSV file header

        //   final String FILE_HEADER = "protein1,protein2,protein3,protein4";


//				        String fileName =new String("/home/om/Music/data/predic_DTI.csv");
        String fileName = new String("D:/drug/opm/predict/result_cuda.csv");

        FileWriter fileWriter = null;
        fileWriter = new FileWriter(fileName);

        //Write the CSV file header
        //  fileWriter.append(FILE_HEADER.toString());


        //Add a new line separator after the header

        // fileWriter.append(NEW_LINE_SEPARATOR);
        for (int i = 0; i < test_Y.length; i++) {
            for (int j = 0; j < test_Y[i].length; j++) {
                fileWriter.write(String.valueOf((test_Y[i][j])));
                fileWriter.append(COMMA_DELIMITER);
            }

            fileWriter.append(NEW_LINE_SEPARATOR);
        }

        fileWriter.flush();

        fileWriter.close();
    }


}
