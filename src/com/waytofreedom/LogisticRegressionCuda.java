package com.waytofreedom;

import org.jblas.FloatMatrix;

import static com.waytofreedom.JCUDAMatrixUtils.multiply;
import static com.waytofreedom.MatrixUtil.mean;
import static com.waytofreedom.MatrixUtil.softmax;

/**
 * Created by rashim on 3/1/17.
 */
public class LogisticRegressionCuda {

    int N;


    //number of inputs from final hidden layer
    private int nIn;
    //number of outputs for labeling
    private int nOut;


    //weight matrix
    protected FloatMatrix W;
    //bias
    protected FloatMatrix b;
    FloatMatrix p_y_given_x;
    FloatMatrix dy;
    FloatMatrix wGradient;
    FloatMatrix wGradientnew;
    FloatMatrix bnew;

    public LogisticRegressionCuda(int N, int nIn, int nOut) {
        this.N = N;
        this.nIn = nIn;
        this.nOut = nOut;


        W = FloatMatrix.zeros(nIn, nOut);
        b = FloatMatrix.zeros(1, nOut);
//        print_matrix(b,"bias printed");

    }

    public FloatMatrix train(FloatMatrix x, FloatMatrix y, float lr) {
        p_y_given_x = softmax(multiply(x, W, false, false).addRowVector(b));
//        System.out.println(y.rows + " and " + y.columns);
//        System.out.println(p_y_given_x.rows + " and " + p_y_given_x.columns);
        dy = y.sub(p_y_given_x);
        dy.divi(x.rows);
        wGradient = multiply(x, dy, true, false);
        wGradientnew = wGradient.muli(lr);
        W.addi(wGradientnew);
        dy.muli(lr);
        dy.divi(x.rows);
        bnew = mean(dy, 0);
        b.addi(bnew);
        return dy;
    }

    public FloatMatrix predict(FloatMatrix x) {
        return softmax(multiply(x, W, false, false).addRowVector(b));
    }


    private static void test_lr() {
        float learning_rate = 0.1f;
        int n_epochs = 1000;

        int train_N = 6;
        int test_N = 2;
        int n_in = 6;
        int n_out = 2;
//
        float[][] train_Xtmp = {
                {1, 1, 1, 0, 0, 0},
                {1, 0, 1, 0, 0, 0},
                {1, 1, 1, 0, 0, 0},
                {0, 0, 1, 1, 1, 0},
                {0, 0, 1, 1, 0, 0},
                {0, 0, 1, 1, 1, 0}
        };
//        float[][] train_Xtmp = {
//                {2.0f, 2.0f, 0.0f, 1.0f, 1.0f, 2.0f},
//                {2.0f, 2.0f, 0.0f, 1.0f, 1.0f, 2.0f},
//                {2.0f, 2.0f, 0.0f, 1.0f, 1.0f, 2.0f},
//                {2.0f, 2.0f, 0.0f, 1.0f, 1.0f, 3.0f},
//                {0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 1.0f},
//                {2.0f, 2.0f, 0.0f, 1.0f, 1.0f, 3.0f}
//        };

        FloatMatrix train_X = new FloatMatrix(train_Xtmp);
        float[][] train_Ytmp = {
                {1, 0},
                {1, 0},
                {1, 0},
                {0, 1},
                {0, 1},
                {0, 1}
        };
        FloatMatrix train_Y = new FloatMatrix(train_Ytmp);

        // construct
        LogisticRegressionCuda classifier = new LogisticRegressionCuda(train_N, n_in, n_out);

        // train
        for (int epoch = 0; epoch < n_epochs; epoch++) {
            classifier.train(train_X, train_Y, learning_rate);

        }

        // test data
        float[][] test_Xtmp = {
                {1, 0, 1, 0, 0, 0},
                {0, 0, 1, 1, 1, 0}
        };

        FloatMatrix test_X = new FloatMatrix(test_Xtmp);

        FloatMatrix test_Y = FloatMatrix.zeros(test_N, n_out);
        // test
        test_Y = classifier.predict(test_X);
        print_matrix(test_Y, "final output");


    }

    public static void print_matrix(FloatMatrix tmp, String txt) {

        System.out.println("**** printing matrix *****" + txt);
        float[][] tmpmatrix = tmp.dup().toArray2();
        for (int y = 0; y < tmpmatrix.length; y++) {
            for (int x = 0; x < tmpmatrix[y].length; x++) {
                System.out.print("  " + tmpmatrix[y][x]);

            }
            System.out.println();
        }

    }

    public static void main(String[] args) {
        test_lr();
    }

}
