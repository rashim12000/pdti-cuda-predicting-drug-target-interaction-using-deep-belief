package com.waytofreedom;


import org.jblas.FloatMatrix;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import static com.waytofreedom.JCUDAMatrixUtils.multiply;
import static com.waytofreedom.MatrixUtil.sigmoid;
import static com.waytofreedom.MatrixUtil.softmax;


/**
 * Created by rashim on 3/1/17.
 */
public class DBNCuda {

    public int N;
    public int n_ins;
    public int[] hidden_layer_sizes;
    public int n_outs;
    public int n_layers;
    public HiddenLayerCuda[] sigmoid_layers;
    public RBMCudaOne[] rbm_layers;
    public LogisticRegressionCuda log_layer;
    public Random rng;
    FloatMatrix layerInput;

    List<FloatMatrix> layer_inputs = new ArrayList<>();
    FloatMatrix prev_W;
    FloatMatrix dy;
    FloatMatrix layer_input;

    FloatMatrix y;
    FloatMatrix linear_output;
    FloatMatrix logistic_layer_dy;
    FloatMatrix prev_dy;
    FloatMatrix prev_layer_input;

    WriteToCSV wc = new WriteToCSV();


    public DBNCuda(int N, int n_ins, int[] hidden_layer_sizes, int n_outs, int n_layers, Random rng) {
        int input_size;

        this.N = N;
        this.n_ins = n_ins;
        this.hidden_layer_sizes = hidden_layer_sizes;
        this.n_outs = n_outs;
        this.n_layers = n_layers;

        this.sigmoid_layers = new HiddenLayerCuda[n_layers];
        this.rbm_layers = new RBMCudaOne[n_layers];

        if (rng == null) this.rng = new Random(1234);
        else this.rng = rng;

        // construct multi-layer
        for (int i = 0; i < this.n_layers; i++) {
            if (i == 0) {
                input_size = this.n_ins;
            } else {
                input_size = this.hidden_layer_sizes[i - 1];
            }

            // construct sigmoid_layer
            this.sigmoid_layers[i] = new HiddenLayerCuda(this.N, input_size, this.hidden_layer_sizes[i], null, null, rng, "sigmoid");

            // construct rbm_layer
            this.rbm_layers[i] = new RBMCudaOne(this.N, input_size, this.hidden_layer_sizes[i], this.sigmoid_layers[i].W, this.sigmoid_layers[i].b, null, rng);
        }

        // layer for output using Logistic Regression
        this.log_layer = new LogisticRegressionCuda(this.N, this.hidden_layer_sizes[this.n_layers - 1], this.n_outs);
    }


    public void pretrain(FloatMatrix input, int k, float lr, int epochs) {


        for (int i = 0; i < n_layers; i++) {
            if (i == 0)
                layerInput = input;
            else {

                layerInput = sigmoid_layers[i - 1].sampleHGivenV(layerInput);

            }


            System.out.println("Training on layer " + (i + 1));
            for (int epoch = 0; epoch < epochs; epoch++) {
                rbm_layers[i].contrastive_divergence(layerInput, lr, k);
            }

        }
    }


    public void finetune(FloatMatrix train_X, FloatMatrix train_Y, float lr, int epochs) {

        for (int epoch = 0; epoch < epochs; epoch++) {

            for (int i = 0; i < n_layers; i++) {
                if (i == 0) {
                    prev_layer_input = train_X;
                } else {
                    prev_layer_input = layer_input.dup();
                }
                layer_inputs.add(prev_layer_input.dup());

                layer_input = sigmoid_layers[i].sampleHGivenV(prev_layer_input);


            }
            logistic_layer_dy = log_layer.train(layer_input, train_Y, lr);

            layer_inputs.add(layer_input.dup());
            // backward hiddenLayers
            FloatMatrix prev_dy = logistic_layer_dy;

            for (int i = n_layers - 1; i >= 0; i--) {

                if (i == n_layers - 1) {
                    prev_W = log_layer.W;

                } else {
                    prev_dy = dy;
                    prev_W = sigmoid_layers[i + 1].W;
                }
                dy = new FloatMatrix(N, hidden_layer_sizes[i]);

                sigmoid_layers[i].backward(layer_inputs.get(i), dy, layer_inputs.get(i + 1), prev_dy, prev_W, lr);
            }
            System.out.print("\repoch = " + (epoch + 1));
        }
    }


    public FloatMatrix predict(FloatMatrix x) {

        FloatMatrix prev_layer_input = x;

        for (int i = 0; i < n_layers; i++) {
            layer_input = new FloatMatrix(N, sigmoid_layers[i].nOut);
         linear_output = new FloatMatrix(x.rows, this.hidden_layer_sizes[i]);
         linear_output = multiply(prev_layer_input, sigmoid_layers[i].W, false, false).addRowVector(sigmoid_layers[i].b);
 layer_input = sigmoid(linear_output);
            if (i < n_layers - 1) {
//                prev_layer_input = new FloatMatrix(x.rows, sigmoid_layers[i].nOut);
                prev_layer_input = layer_input;
            }

        }

        y = multiply(layer_input, log_layer.W, false, false).addRowVector(log_layer.b);

        return softmax(y);

    }

    private static void test_dbn() throws IOException {
        Random rng = new Random(123);

        //hyper-parameters
        float pretrain_lr = 0.05f;
        int pretraining_epochs = 5;
        int k = 1;
        float finetune_lr = 0.01f;
        int finetune_epochs = 10000;

        int train_N = 6;
        int test_N = 4;
        int n_ins = 12;
        int n_outs = 4;
        int[] hidden_layer_sizes = {600, 500, 400};
        int n_layers = hidden_layer_sizes.length;
//
//        //for mnist data testing
//        int train_N = 1007;
//        int test_N = 10;
//        int n_ins = 881;
//        int n_outs = 775;
//        int[] hidden_layer_sizes = {2000, 1990, 1980};
//        int n_layers = hidden_layer_sizes.length;


        // construct DNN.DBN
        DBNCuda DBNCuda = new DBNCuda(train_N, n_ins, hidden_layer_sizes, n_outs, n_layers, rng);

//         data loading
        System.out.println("Data Loading.................");

//        float[][] train_X1 = new float[1007][881];
//        ReadCSV r1 = new ReadCSV();
//        r1.read_X(train_X1);

//        double[][] train_X = new double[50][881];
//        int m = 50, n = 0;
//        while (n < m) {
//            for (int i = 0; i < 50; i++) {
//                for (int j = 0; j < 881; j++) {
//                    train_X[i][j] = train_X1[n][j];
//                }
//                n++;
//            }
//
//            System.out.println("training by mini-batch" + n / 50 + "................");
//         training data
        float[][] train_Xtmp = {
                {1, 1, 1, 0, 0, 0, 1, 1, 1, 0, 0, 0},
                {1, 0, 1, 0, 0, 0, 1, 1, 1, 0, 0, 0},
                {1, 1, 1, 0, 0, 0, 1, 1, 1, 0, 0, 0},
                {0, 0, 1, 1, 1, 0, 1, 1, 1, 0, 0, 0},
                {0, 0, 1, 1, 0, 0, 1, 1, 1, 0, 0, 0},
                {0, 0, 1, 1, 1, 0, 1, 1, 1, 0, 0, 0}
        };
//        FloatMatrix train_X = new FloatMatrix(train_X1);
        FloatMatrix train_X = new FloatMatrix(train_Xtmp);
        // pretrain
        DBNCuda.pretrain(train_X, k, pretrain_lr, pretraining_epochs);

//
        System.out.println("Pre-Training Finished.");
//
        System.out.println("data loading for fine tuning.....");

//        float[][] train_Y2 = new float[1007][775];
//        float[][] train_Y1 = new float[1007][775];
//        ReadCSV r2 = new ReadCSV();
//        r2.read_Y(train_Y1);
//
//        for (int i = 0; i < 100; i++) {
//            for (int j = 0; j < 775; j++) {
//                train_Y2[i][j] = train_Y1[i][j];
//            }
//        }

        float[][] train_Y2 = {
                {1, 0, 0, 0},
                {1, 1, 0, 1},
                {0, 0, 1, 0},
                {0, 0, 0, 1},
                {0, 1, 0, 0},
                {0, 1, 1, 0},
        };
        // finetune

//        float[][] train_Xtmp = {
//                {1, 1, 1, 0, 0, 0, 1, 1, 1, 0, 0, 0},
//                {1, 0, 1, 0, 0, 0, 1, 1, 1, 0, 0, 0},
//                {1, 1, 1, 0, 0, 0, 1, 1, 1, 0, 0, 0},
//                {0, 0, 1, 1, 1, 0, 1, 1, 1, 0, 0, 0},
//                {0, 0, 1, 1, 0, 0, 1, 1, 1, 0, 0, 0},
//                {0, 0, 1, 1, 1, 0, 1, 1, 1, 0, 0, 0}
//        }; float[][] test_Xtmp = {
//        {1, 1, 0, 0, 0, 0, 1, 1, 0, 0, 0, 0},
//        {1, 1, 1, 1, 0, 0, 1, 1, 0, 0, 0, 0},
//        {0, 0, 0, 1, 1, 0, 1, 1, 0, 0, 0, 0},
//        {0, 0, 1, 1, 1, 0, 1, 1, 0, 0, 0, 0},
//    };
        System.out.println("Fine tuning.................");
        FloatMatrix train_Y = new FloatMatrix(train_Y2);
        DBNCuda.finetune(train_X, train_Y, finetune_lr, finetune_epochs);

        /*
        double [][]test_X=new double[10][881];
		for(int i=0;i<10;i++)
		{
		  for (int j=0;j<881;j++)
		  {
			  test_X[i][j]=train_X1[i][j];
		  }
		}
        */
        // test data
        float[][] test_Xtmp = {
                {1, 1, 0, 0, 0, 0, 1, 1, 0, 0, 0, 0},
                {1, 1, 1, 1, 0, 0, 1, 1, 0, 0, 0, 0},
                {0, 0, 0, 1, 1, 0, 1, 1, 0, 0, 0, 0},
                {0, 0, 1, 1, 1, 0, 1, 1, 0, 0, 0, 0},
        };
//        float[][] test_X1 = new float[99][785];
//        ReadCSV r3 = new ReadCSV();
//        r3.read_test_X(test_X1);
//

//        float[][] test_Xtmp = new float[10][881];
//        for (int i = 0; i < 10; i++) {
//            for (int j = 0; j < 881; j++) {
//                test_Xtmp[i][j] = train_X1[i][j];
//            }
//        }

//        float[][] test_Xtmp = new float[6][881];
//        ReadCSV r3 = new ReadCSV();
//        r1.read_X_test(test_Xtmp);

        FloatMatrix test_X = new FloatMatrix(test_Xtmp);
        FloatMatrix test_Y = new FloatMatrix(test_N, n_outs);
        System.out.println();
        System.out.println("Testing......................\n\n.");
        // test
//        for (int i = 0; i < test_N; i++) {
        test_Y = DBNCuda.predict(test_X);
        System.out.println("final op");
        float[][] tmpmatrix = test_Y.dup().toArray2();
        for (int y = 0; y < tmpmatrix.length; y++) {
            for (int x = 0; x < tmpmatrix[y].length; x++) {
                System.out.print("  " + tmpmatrix[y][x]);

            }
            System.out.println();
        }
//        }

        WriteToCSV wc = new WriteToCSV();
        wc.write(test_Y);
    }

    public static void main(String[] args) throws IOException {
        long start = System.currentTimeMillis();
        test_dbn();
        System.out.println("GPU took: " + (System.currentTimeMillis() - start) / 1000f + "s!");
    }
}
