package com.waytofreedom;

import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

/**
 * Created by Rashim12000 on 7/7/2017.
 */
public class ReadStructure {
    public ReadStructure() {

    }

    //    D:/drug/opm/drug_structure_test.csv
    public void readdata(String[] train_X) throws FileNotFoundException {
        //int[][] train_X = new int[1007][881];
        //int[][] train_X = new int[1007][775];

        String delimiter = ",";
        int c = 0;
        int row = 0;
        int count = 0;
        Scanner sc = new Scanner(new File("D:/drug/opm/drug_structure_test.csv"));
//        Scanner sc = new Scanner(new File("D:/mnist/mnist_train.csv"));
//        Scanner sc = new Scanner(new File("D:/mnist/mnist_train_min.csv"));

        while (sc.hasNextLine()) {
            String line = sc.nextLine();
            String[] testStr = line.split(delimiter);
            for (int x = 0; x < testStr.length; x++) {

                if (x == 0) {
//                    System.out.println(row);
//                    System.out.println((testStr[x]));
                    train_X[row] = (testStr[x]);
                }

            }
            row++;

//            count++;
        }
//        System.out.println();
//        int i, j = 0;
//        for (i = 0; i < 1007; i++) {
//            for (j = 0; j < 881; j++) {
//                System.out.print("i=" + i + "j=" + j + "     " + train_X[i][j] + "    ");
//            }
//            System.out.println();
//        }

    }


    public void readproteintitle(String[] train_X) throws IOException {
        String delimiter = ",";
        int c = 0;
        int row = 0;
        int count = 0;
        Scanner sc = new Scanner(new File("D:/drug/opm/drug_protein_test.csv"));
//        Scanner sc = new Scanner(new File("D:/mnist/mnist_train.csv"));
//        Scanner sc = new Scanner(new File("D:/mnist/mnist_train_min.csv"));


        String line = sc.nextLine();
        String[] testStr = line.split(delimiter);

//        System.out.print(testStr.length);
        for (int x = 0; x < testStr.length; x++) {
//            System.out.println(testStr[x]);
            train_X[c] = (testStr[x]);
            c++;
        }


//            count++;
        //        System.out.println();
//        int i, j = 0;
//        for (i = 0; i < 1007; i++) {
//            for (j = 0; j < 881; j++) {
//                System.out.print("i=" + i + "j=" + j + "     " + train_X[i][j] + "    ");
//            }
//            System.out.println();
//        }


    }

    public void readrow(float[][] train_X) throws IOException {

        String delimiter = ",";
        int c = 0;
        int row = 0;
        Scanner sc = null;
        try {
            sc = new Scanner(new File("D:/drug/opm/drug_structure_test.csv"));
        } catch (FileNotFoundException e1) {
            e1.printStackTrace();
        }
//        Scanner sc = new Scanner(new File("D:/mnist/mnist_train.csv"));
//        Scanner sc = new Scanner(new File("D:/mnist/mnist_train_min.csv"));

        while (sc.hasNextLine()) {
            String line = sc.nextLine();
            String[] testStr = line.split(delimiter);
            for (int x = 0; x < testStr.length; x++) {
                if (c == 0)
                    continue;
                if (x != 0)
                    train_X[row][x - 1] = Integer.parseInt(testStr[x]);

            }
            if (c == 0) {
                c++;
            } else {
                row++;
            }

        }
//                System.out.println();
//                int i, j = 0;
////        for (i = 0; i < 1007; i++) {
//                for (j = 0; j < 881; j++) {
//                    System.out.print( "j=" + j + "     " + train_X[varname-1][j] + "    ");
//                }
//                System.out.println();
////        }

    }

    public void readrowprotein(float[][] train_X) throws IOException {

        String delimiter = ",";
        int c = 0;
        int row = 0;
        Scanner sc = null;
        try {
            sc = new Scanner(new File("D:/drug/opm/drug_protein_test.csv"));
        } catch (FileNotFoundException e1) {
            e1.printStackTrace();
        }
//        Scanner sc = new Scanner(new File("D:/mnist/mnist_train.csv"));
//        Scanner sc = new Scanner(new File("D:/mnist/mnist_train_min.csv"));

        while (sc.hasNextLine()) {
            String line = sc.nextLine();
            String[] testStr = line.split(delimiter);
            for (int x = 0; x < testStr.length; x++) {
                if (c == 0)
                    continue;
                if (x != 0)
                    train_X[row][x - 1] = Integer.parseInt(testStr[x]);

            }
            if (c == 0) {
                c++;
            } else {
                row++;
            }

        }
//                System.out.println();
//                int i, j = 0;
////        for (i = 0; i < 1007; i++) {
//                for (j = 0; j < 881; j++) {
//                    System.out.print( "j=" + j + "     " + train_X[varname-1][j] + "    ");
//                }
//                System.out.println();
////        }

    }

    public static void main(String[] args) throws IOException {
        String[] ptitle = new String[775];
        ReadStructure readStructure = new ReadStructure();
        readStructure.readproteintitle(ptitle);
    }
}
