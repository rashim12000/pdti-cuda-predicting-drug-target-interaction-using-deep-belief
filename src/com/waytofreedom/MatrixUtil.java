package com.waytofreedom;

/**
 * Created by rashim on 2/18/17.
 */

import org.jblas.DoubleMatrix;
import org.jblas.FloatMatrix;
import org.jblas.MatrixFunctions;

import java.util.Random;

import static com.waytofreedom.LogisticRegressionCuda.print_matrix;
import static org.jblas.FloatMatrix.ones;

public class MatrixUtil {


    public static float uniform(double min, double max, Random rng) {
        return (float) (rng.nextFloat() * (max - min) + min);
    }

//    public static int binomial(int n, float p, Random rng) {
//        if(p < 0 || p > 1) return 0;
//
//        int c = 0;
//        double r;
//
//        for(int i=0; i<n; i++) {
//            r = rng.nextFloat();
//            if (r < p) c++;
//        }
//
//        return c;
//    }


    public static FloatMatrix binomial(FloatMatrix p, int n, Random rng) {
        FloatMatrix ret = p;
        for (int i = 0; i < p.length; i++) {

//            System.out.println("binomila=== "+utils.binomialmatrix(n, p.get(i),rng));
            ret.put(i, utils.binomial(n, p.get(i), rng));
        }
        return ret;
    }


    public static FloatMatrix sigmoid(FloatMatrix x) {
        FloatMatrix ones = ones(x.rows, x.columns);
        FloatMatrix exp = MatrixFunctions.exp(x.neg());
        return ones.div(ones.add(exp));
    }

    public static FloatMatrix dsigmoid(FloatMatrix x) {
//        FloatMatrix ones = FloatMatrix.ones(x.rows, x.columns);
//        FloatMatrix ones = FloatMatrix.ones(x.rows, x.columns);
//        return ones.div(ones.add(MatrixFunctions.exp(x.neg())));
FloatMatrix om = oneMinus(x);
        return x.mul(om);


    }


    public static FloatMatrix softmax(FloatMatrix input) {
        FloatMatrix max = input.rowMaxs();
        FloatMatrix diff = MatrixFunctions.exp(input.subColumnVector(max));
        diff.diviColumnVector(diff.rowSums());
        return diff;
    }

    public static FloatMatrix oneMinus(FloatMatrix ep) {
        FloatMatrix on = ones(ep.rows, ep.columns);
        return on.sub(ep);
    }

    public static FloatMatrix mean(FloatMatrix input, int axis) {
        FloatMatrix ret = new FloatMatrix(input.rows, 1);
        //column wise
        if (axis == 0) {
            return input.columnMeans();
        }
        //row wise
        else if (axis == 1) {
            return ret.rowMeans();
        }
        return ret;
    }

    public static double dsigmoid(double x) {
        return x * (1. - x);
    }

    public static double tanh(double x) {
        return Math.tanh(x);
    }

    public static double dtanh(double x) {
        return 1. - x * x;
    }

    public static double ReLU(double x) {
        if (x > 0) {
            return x;
        } else {
            return 0.;
        }
    }

    public static double dReLU(double x) {
        if (x > 0) {
            return 1.;
        } else {
            return 0.;
        }
    }
}
