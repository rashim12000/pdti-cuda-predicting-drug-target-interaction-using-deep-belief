package com.waytofreedom;

import activation.ActivationFunction;
import org.jblas.DoubleMatrix;
import org.jblas.FloatMatrix;

import java.util.Random;

import static com.waytofreedom.JCUDAMatrixUtils.multiply;
import static com.waytofreedom.MatrixUtil.*;
import static com.waytofreedom.utils.binomial;

/**
 * Created by rashim on 2/23/17.
 */
public class HiddenLayerCuda {
    public int N;
    public int nIn;
    public int nOut;
    public FloatMatrix W;
    public FloatMatrix b;
    public Random rng;
    public FloatMatrix input;
    public ActivationFunction<FloatMatrix> activation;
    public ActivationFunction<FloatMatrix> dactivation;
    FloatMatrix output;
    FloatMatrix sample;
    FloatMatrix dytmp;
    FloatMatrix dyxx;
    FloatMatrix wgradient;
    FloatMatrix bnew;
    FloatMatrix linear_output;

    public HiddenLayerCuda() {
    }


    public HiddenLayerCuda(int N, int nIn, int nOut, FloatMatrix W, FloatMatrix b, Random rng, String activation) {
        this.N = N;
        this.nIn = nIn;
        this.nOut = nOut;
        if (rng == null) {
            this.rng = new Random(1234);
        } else
            this.rng = rng;
        if (W == null) {

            float[][] Wtemp = new float[this.nIn][this.nOut];
            double a = 1.0 / this.nIn;
            for (int i = 0; i < this.nIn; i++) {
                for (int j = 0; j < this.nOut; j++) {
                    Wtemp[i][j] = uniform(-a, a, rng);
                }
            }

            this.W = new FloatMatrix(Wtemp);
        } else
            this.W = W;


        if (b == null)
            this.b = FloatMatrix.zeros(nOut);
        else
            this.b = b;


        if (activation == "sigmoid" || activation == null) {
            this.activation = (FloatMatrix x) -> sigmoid(x);
            this.dactivation = (FloatMatrix x) -> dsigmoid(x);

        } else {
            throw new IllegalArgumentException("activation function not supported");
        }

    }

    public FloatMatrix output(FloatMatrix input) {
        linear_output = multiply(input, W, false, false).addiRowVector(b);
        return activation.apply(linear_output);
    }

    public FloatMatrix sampleHGivenV(FloatMatrix input) {

        sample = MatrixUtil.binomial(output(input), 1, rng);
        return sample;
    }

    public static void print_matrix(FloatMatrix tmp, String txt) {

        System.out.println("**** printing matrix *****" + txt);
        float[][] tmpmatrix = tmp.dup().toArray2();
        for (int y = 0; y < tmpmatrix.length; y++) {
            for (int x = 0; x < tmpmatrix[y].length; x++) {
                System.out.print("  " + tmpmatrix[y][x]);

            }
            System.out.println();

        }
    }

    public void backward(FloatMatrix input, FloatMatrix dy, FloatMatrix prev_layer_input, FloatMatrix prev_layer_dy, FloatMatrix prev_layer_W, float lr) {
        if (dy == null) dy = new FloatMatrix(nIn, nOut);
        dytmp = multiply(prev_layer_dy, prev_layer_W, false, true);
        dy.addi(dytmp);
        dyxx = dactivation.apply(prev_layer_input);
        dy.mul(dyxx);
        wgradient = (multiply(input, dy, true, false));
        wgradient.div(N);
        wgradient.muli(lr);
        W.addi(wgradient.transpose());
        bnew = mean(dy.muli(lr), 0);
        b.addi(bnew);
    }
}
