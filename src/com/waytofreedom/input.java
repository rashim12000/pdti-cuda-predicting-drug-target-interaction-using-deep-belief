package com.waytofreedom;


import org.jblas.FloatMatrix;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;

import static com.waytofreedom.HiddenLayerCuda.print_matrix;

/**
 * Created by Rashim12000 on 7/7/2017.
 */
public class input {
    private JComboBox drug_input;
    private JPanel panel1;
    private JList list1;
    String[] structure = new String[9];
    String rowdata;


    input() throws FileNotFoundException {
        JFrame jf = new JFrame("input");
        ReadStructure rs = new ReadStructure();

        rs.readdata(structure);

//        for (int i = 0; i < 4; i++) {
////            int j = 0;
//            System.out.println(structure[i]);
//        }
        drug_input = new JComboBox(structure);
        JButton btn_predict = new JButton("Predict");
        drug_input.setBounds(50, 50, 90, 20);
        jf.add(drug_input);

        btn_predict.setBounds(150, 50, 90, 20);
        jf.add(btn_predict);

        list1 = new JList();
//        jf.add(list1);
//        list1.setBounds(50, 200, 190, 90);
        jf.setLayout(null);
        jf.setSize(300, 200);
        jf.setVisible(true);

        btn_predict.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                int varname = (int) drug_input.getSelectedIndex();
                System.out.println(varname);
                float[][] train_X = new float[9][881];
                float[][] test_protein = new float[9][775];
                float[][] predcited_op = new float[1][775];
                float[][] actual_op = new float[1][775];


                try {
                    rs.readrow(train_X);
//
//                    float[][] result = new float[1][881];
//                    FloatMatrix test_XX = new FloatMatrix(train_X[varname - 1]);
////                    print_matrix(test_XX,"sdfasdf");
//                    float[] test_XXX = test_XX.dup().toArray();
//                    for (int x = 0; x < test_XXX.length; x++) {
//                        System.out.print("  " + test_XXX[x]);
//
//                    }
//                    for (int i = 0; i < test_XX.getRows(); i++) {
//                        for (int j = 0; j < test_XX.getColumns(); j++) {
////                            System.out.print(varname);
//                            if (i == varname) {
//                                System.out.print(test_XX.get(i-1, j));
//                                result[0][j] = test_XX.get(i-1, j);
//                                System.out.print(" " + result[0][j] + " ");
//                            }
//                        }
//                    }
                    FloatMatrix test_XX = new FloatMatrix(train_X);
                    Predict_drug pd = new Predict_drug();
                    FloatMatrix test_Y = new FloatMatrix();
//                    FloatMatrix test_XXX = new FloatMatrix(result);

                    test_Y = pd.predict(test_XX);
                    WriteToCSV wc = new WriteToCSV();
                    try {
                        wc.write_drug_predict(test_Y);
                    } catch (IOException f) {
                        f.printStackTrace();
                    }
//                    System.out.println("final op");
                    float[][] tmpmatrix = test_Y.dup().toArray2();
//                    for (int y = 0; y < tmpmatrix.length; y++) {
                    for (int x = 0; x < tmpmatrix[varname - 1].length; x++) {
//                            if (y == varname)

                        predcited_op[0][x] = tmpmatrix[varname - 1][x];
                        //thresholding
                        if (predcited_op[0][x] > 0.05) {
                            predcited_op[0][x] = 1;
                        } else {
                            predcited_op[0][x] = 0;
                        }
//                        System.out.print("  " + predcited_op[0][x]);
                    }
                    System.out.println();
//                    }

                    rs.readrowprotein(test_protein);
                    FloatMatrix test_p = new FloatMatrix(test_protein);
//                    float result_p[][] = new float[1][775];
//                    System.out.println("real protein");
                    float[][] tmpmatrixprotein = test_p.dup().toArray2();
//                    for (int y = 0; y < tmpmatrixprotein.length; y++) {
                    for (int x = 0; x < tmpmatrixprotein[varname - 1].length; x++) {
//                            if (y == varname)
//                                System.out.print("  " + tmpmatrixprotein[y][x]);
                        actual_op[0][x] = tmpmatrixprotein[varname - 1][x];
//                        System.out.print("  " + actual_op[0][x]);
                    }
                    System.out.println();
//                    }
//                    System.out.println();

//                    for (int i = 0; i < test_p.getRows(); i++) {
//                        for (int j = 0; j < test_p.getColumns(); j++) {
////                            System.out.print(varname);
//                            if (i == varname) {
////                                System.out.print(test_p.get(i, j));
//                                result_p[0][j] = test_p.get(i, j);
//                                System.out.print(" " + result_p[0][j] + " ");
//                            }
//                        }
//                    }
//                    FloatMatrix result_pp = new FloatMatrix(result_p);

//
//                    for (int j = 0; j < predcited_op[0].length; j++) {
//                        System.out.print(" " + predcited_op[0][j] + " ");
////                    }
//                    System.out.println();
//                    for (int j = 0; j < actual_op[0].length; j++) {
//                        System.out.print(" " + actual_op[0][j] + " ");
//                    }
//                    System.out.println();

                    float count = 0;
                    float TP = 0;
                    float FP = 0;
                    float TN = 0;
                    float FN = 0;

//                    float[] test_YY = test_p.dup().toArray();
//                    for (int y = 0; y < tmpmatrix.length; y++) {
//                    System.out.println(actual_op[0].length);
//                    System.out.println(predcited_op[0].length);
                    for (int x = 0; x < actual_op[0].length; x++) {
                        if ((predcited_op[0][x]) == actual_op[0][x]) {
//                            System.out.println(count++);

                            count++;
                        }
                        if (actual_op[0][x] == 0 && predcited_op[0][x] == 0) {
                            TN++;

                        }
                        if (actual_op[0][x] == 0 && predcited_op[0][x] == 1) {
                            FP++;
                        }
                        if (actual_op[0][x] == 1 && predcited_op[0][x] == 0) {
                            FN++;
                        }
                        if (actual_op[0][x] == 1 && predcited_op[0][x] == 1) {
                            TP++;
                        }
                    }
//                    }

//                    System.out.println("TP=" + TP);
//                    System.out.println("TN=" + TN);
//                    System.out.println("FN=" + FN);
//                    System.out.println("FP=" + FP);
//                    System.out.println("Count=" + count);
                    float T = TP + TN + FP + FN;
                    float sensitivity = TP / (TP + FN);
                    float specificity = TN / (TN + FP);
                    float precision = TP / (TP + FP);
                    float acc = (TP + TN) / T;
                    float f1 = 2 * TP / (2 * TP + FP + FN);


//                    System.out.println("sensitivity = " + sensitivity);
//                    System.out.println("specificity = " + specificity);
//                    System.out.println("precision = " + precision);
//                    System.out.println("acc = " + acc);
//                    System.out.println("f1 = " + f1);

                    String[] ptitle = new String[776];
                    ReadStructure readStructure = new ReadStructure();
                    readStructure.readproteintitle(ptitle);
                    int h = 0;
                    ArrayList<String> interact = new ArrayList<>();
                    for (int u = 0; u < 775; u++) {
                        if (predcited_op[0][u] == 1) {
                            interact.add(ptitle[u+1]);
                        }
                    }
                    System.out.println(Arrays.toString(interact.toArray()));
//                    float accuracy = (count / 775) * 100;
//                    System.out.println("accuracy is " + accuracy + "%");
                    list1 = new JList(interact.toArray());
                    list1.setVisibleRowCount(5);

                    JOptionPane.showMessageDialog(null, new JScrollPane(list1));
                } catch (IOException e1) {
                    e1.printStackTrace();
                }

            }
        });


    }


    public static void main(String[] args) throws IOException {
        new input();


    }


}
