package com.waytofreedom;

import org.jblas.FloatMatrix;

import java.io.FileNotFoundException;
import java.io.IOException;

import static com.waytofreedom.HiddenLayerCuda.print_matrix;
import static com.waytofreedom.JCUDAMatrixUtils.multiply;
import static com.waytofreedom.MatrixUtil.sigmoid;
import static com.waytofreedom.MatrixUtil.softmax;

/**
 * Created by Rashim12000 on 6/14/2017.
 */
public class Predict_one {


    public FloatMatrix predict(FloatMatrix x) throws FileNotFoundException {
        ReadCSV r = new ReadCSV();
        FloatMatrix layer_input = new FloatMatrix();
        FloatMatrix prev_layer_input = x;
        FloatMatrix y;
        FloatMatrix linear_output = new FloatMatrix();

        System.out.println("loading data");


        float[][] sigmoid_zero = new float[881][800];

        r.read_weight(sigmoid_zero, "D:/drug/opm/predict/answertwo5000/resultweight0.csv");

//        float[][] sigmoid_one = new float[2000][1990];
//
//        r.read_weight(sigmoid_one, "D:/drug/opm/predict/answertwo5000/resultweight1.csv");
//        float[][] sigmoid_two = new float[1990][1980];
//
//        r.read_weight(sigmoid_two, "D:/drug/opm/predict/answertwo5000/resultweight2.csv");

        float[][] bias_zero = new float[800][1];

        r.read_weight(bias_zero, "D://drug//opm//predict/answertwo5000//resultbias0.csv");

//        float[][] bias_one = new float[1990][1];
//
//        r.read_weight(bias_one, "D:/drug/opm/predict/answertwo5000/resultbias1.csv");
//        float[][] bias_two = new float[1980][1];
//        r.read_weight(bias_two, "D:/drug/opm/predict/answertwo5000/resultbias2.csv");

        float[][] log = new float[800][775];
        r.read_weight(log, "D:/drug/opm/predict/answertwo5000/resultloglayer.csv");

        float[][] log_b = new float[1][775];
        r.read_weight(log, "D:/drug/opm/predict/answertwo5000/resultloglayerbias.csv");

        FloatMatrix sigmoid_0 = new FloatMatrix(sigmoid_zero);
//        FloatMatrix sigmoid_1 = new FloatMatrix(sigmoid_one);
//        FloatMatrix sigmoid_2 = new FloatMatrix(sigmoid_two);

        FloatMatrix bias_0 = new FloatMatrix(bias_zero);
//        FloatMatrix bias_1 = new FloatMatrix(bias_one);
//        FloatMatrix bias_2 = new FloatMatrix(bias_two);

        FloatMatrix log_weight = new FloatMatrix(log);

        FloatMatrix log_bias = new FloatMatrix(log_b);
//        print_matrix(log_bias, "log wi");
        System.out.println("loading data finished");
//        for (int i = 0; i < 3; i++) {
//            if (i == 0) {
                linear_output = multiply(prev_layer_input, sigmoid_0, false, false).addRowVector(bias_0);
//            } else if (i == 1) {
//                linear_output = multiply(prev_layer_input, sigmoid_1, false, false).addRowVector(bias_1);
//
//            } else {
//                linear_output = multiply(prev_layer_input, sigmoid_2, false, false).addRowVector(bias_2);
//
//            }
            layer_input = sigmoid(linear_output);


//            if (i < 3 - 1) {
//                //opt
////                prev_layer_input = new FloatMatrix(x.rows, sigmoid_layers[i].nOut);
//                prev_layer_input = layer_input;
//            }

//        }
//                    print_matrix(layer_input, "layer inp");
//            print_matrix(log_layer.W, "log W");


        y = multiply(layer_input, log_weight, false, false).addRowVector(log_bias);
//            print_matrix(y,"final y");
        return softmax(y);
//        return y;
    }

    public static void main(String[] args) throws FileNotFoundException {
        Predict_one pd = new Predict_one();

        float[][] test_X = new float[1][881];
        ReadCSV r10 = new ReadCSV();
        r10.read_predict(test_X);
        FloatMatrix test_XX = new FloatMatrix(test_X);
        FloatMatrix test_Y = new FloatMatrix();
        print_matrix(test_XX,"test");
        test_Y = pd.predict(test_XX);
        System.out.println("final op");
        float[][] tmpmatrix = test_Y.dup().toArray2();
        for (int y = 0; y < tmpmatrix.length; y++) {
            for (int x = 0; x < tmpmatrix[y].length; x++) {
                System.out.print("  " + tmpmatrix[y][x]);

            }
            System.out.println();
            System.out.println();
        }
//        }

        WriteToCSV wc = new WriteToCSV();
        try {
            wc.write_drug_predict(test_Y);
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

}
