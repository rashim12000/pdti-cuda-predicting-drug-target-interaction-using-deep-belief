package com.waytofreedom;


import org.jblas.FloatMatrix;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import static com.waytofreedom.JCUDAMatrixUtils.multiply;
import static com.waytofreedom.MatrixUtil.sigmoid;
import static com.waytofreedom.MatrixUtil.softmax;


/**
 * Created by rashim on 3/1/17.
 */
public class DBNBatch {

    public int N;
    public int n_ins;
    public int[] hidden_layer_sizes;
    public int n_outs;
    public int n_layers;
    public HiddenLayerCuda[] sigmoid_layers;
    public RBMCudaOne[] rbm_layers;
    public LogisticRegressionCuda log_layer;
    public Random rng;
    FloatMatrix layerInput;


    FloatMatrix prev_W;
    FloatMatrix y;
    FloatMatrix linear_output;
    FloatMatrix logistic_layer_dy;
    FloatMatrix prev_dy;
    FloatMatrix prev_layer_input;

    WriteToCSV wc = new WriteToCSV();

    public DBNBatch(int N, int n_ins, int[] hidden_layer_sizes, int n_outs, int n_layers, Random rng) {
        int input_size;

        this.N = N;
        this.n_ins = n_ins;
        this.hidden_layer_sizes = hidden_layer_sizes;
        this.n_outs = n_outs;
        this.n_layers = n_layers;

        this.sigmoid_layers = new HiddenLayerCuda[n_layers];
        this.rbm_layers = new RBMCudaOne[n_layers];

        if (rng == null) this.rng = new Random(1234);
        else this.rng = rng;

        // construct multi-layer
        for (int i = 0; i < this.n_layers; i++) {
            if (i == 0) {
                input_size = this.n_ins;
            } else {
                input_size = this.hidden_layer_sizes[i - 1];
            }

            // construct sigmoid_layer
            this.sigmoid_layers[i] = new HiddenLayerCuda(this.N, input_size, this.hidden_layer_sizes[i], null, null, rng, "sigmoid");

            // construct rbm_layer
            this.rbm_layers[i] = new RBMCudaOne(this.N, input_size, this.hidden_layer_sizes[i], this.sigmoid_layers[i].W, this.sigmoid_layers[i].b, null, rng);
        }

        // layer for output using Logistic Regression
        this.log_layer = new LogisticRegressionCuda(this.N, this.hidden_layer_sizes[this.n_layers - 1], this.n_outs);

//        for (int i=0;i<this.sigmoid_layers.length;i++){
//            System.out.println(sigmoid_layers[i].toString());
//        }
    }


    public void pretrain(float[][] input_f, int k, float lr, int epochs) {
        FloatMatrix input = new FloatMatrix(input_f);
        FloatMatrix layerInput = null;

        for (int i = 0; i < n_layers; i++) {
            if (i == 0)
                layerInput = input;
            else {
                layerInput = sigmoid_layers[i - 1].sampleHGivenV(layerInput);
            }
            System.out.println("Training on layer " + (i + 1));
            for (int epoch = 0; epoch < epochs; epoch++) {
                System.out.print("\repoch = " + (epoch + 1));
                rbm_layers[i].contrastive_divergence(layerInput, lr, k);
            }
        }
    }


    public void finetune(float[][] train_Xf, FloatMatrix train_Y, float lr, int epochs) {
        FloatMatrix train_X = new FloatMatrix(train_Xf);

        for (int epoch = 0; epoch < epochs; epoch++) {
            List<FloatMatrix> layer_inputs = new ArrayList<>();
            FloatMatrix layer_input = null;

            for (int i = 0; i < n_layers; i++) {
                if (i == 0) {
                    prev_layer_input = train_X;
                } else {
                    prev_layer_input = layer_input;
                }
                layer_inputs.add(prev_layer_input.dup());

                layer_input = sigmoid_layers[i].sampleHGivenV(prev_layer_input);

            }
//            System.out.println(layer_input.rows + " and "+ layer_input.columns);
//            System.out.println(train_Y.rows + " and "+ train_Y.columns);
            logistic_layer_dy = log_layer.train(layer_input, train_Y, lr);


            layer_inputs.add(layer_input.dup());
            // backward hiddenLayers
            FloatMatrix dy = null;
            prev_dy = logistic_layer_dy;
            for (int i = n_layers - 1; i >= 0; i--) {

                if (i == n_layers - 1) {
                    prev_W = log_layer.W;

                } else {
                    prev_dy = dy;
                    prev_W = sigmoid_layers[i + 1].W;
                }
                dy = new FloatMatrix(N, hidden_layer_sizes[i]);
                sigmoid_layers[i].backward(layer_inputs.get(i), dy, layer_inputs.get(i + 1), prev_dy, prev_W, lr);
            }
            System.out.print("\repoch = " + (epoch + 1));
        }
    }


    public void predict() {
//        FloatMatrix layer_input = null;
//        prev_layer_input = x;


        for (int i = 0; i < n_layers; i++) {

            try {
                wc.write_weight(sigmoid_layers[i].W, i);
                wc.write_bias(sigmoid_layers[i].b, i);
            } catch (IOException e) {
                e.printStackTrace();
            }
//            linear_output = multiply(prev_layer_input, sigmoid_layers[i].W, false, false).addRowVector(sigmoid_layers[i].b);
//            layer_input = sigmoid(linear_output);
//
//
//            if (i < n_layers - 1) {
//                prev_layer_input = layer_input;
//            }

        }

        try {
            wc.write_loglayer(log_layer.W);
            wc.write_loglayerbias(log_layer.b);
        } catch (IOException e) {
            e.printStackTrace();
        }
//        y = multiply(layer_input, log_layer.W, false, false).addRowVector(log_layer.b);
//        return softmax(y);
    }


    private static void test_dbn() throws IOException {
        Random rng = new Random(123);

        //hyper-parameters
        float pretrain_lr = 0.1f;
        int pretraining_epochs = 1;
        int k = 1;
        float finetune_lr = 0.1f;
        int finetune_epochs = 1;

//        int train_N = 6;
//        int test_N = 4;
//        int n_ins = 12;
//        int n_outs = 4;
//        int[] hidden_layer_sizes = {6, 5, 4};
//        int n_layers = hidden_layer_sizes.length;

        //for mnist data testing
        int train_N = 991;
        int test_N = 6;
        int n_ins = 881;
        int n_outs = 775;
        int[] hidden_layer_sizes = {2000, 1990, 1980};
//        int[] hidden_layer_sizes = {1000,990,980};
        int n_layers = hidden_layer_sizes.length;
//        System.out.println(n_layers);

        // construct DNN.DBN
        DBNBatch DBNBatch = new DBNBatch(train_N, n_ins, hidden_layer_sizes, n_outs, n_layers, rng);

//         data loading
        System.out.println("Data Loading.................");

        float[][] train_X1 = new float[991][881];
        ReadCSV r1 = new ReadCSV();
        r1.read_X(train_X1);

//        FloatMatrix train_X = new FloatMatrix(train_X1);
//        train_X1 = null;
        // pretrain

//        FloatMatrix train_X2 = null;
        float[][] train_X = new float[991][881];
        int m = 900, n = 0;
        while (n < m) {
            for (int i = 0; i < 100; i++) {
                for (int j = 0; j < 881; j++) {
                    train_X[i][j] = train_X1[n][j];
//                train_X[i][j] = train_X1[i][j];
                }
                n++;
            }
//             train_X2 = new FloatMatrix(train_X);

//            FloatMatrix train_X3 = new FloatMatrix(train_X2);
            System.out.println("training by mini-batch" + n / 100 + "................");

            // pretrain
            DBNBatch.pretrain(train_X, k, pretrain_lr, pretraining_epochs);
        }
        System.out.println("Pre-Training Finished.");
        System.out.println("data loading for fine tuning.....");

        float[][] train_Y1 = new float[991][775];
        ReadCSV r2 = new ReadCSV();
        r2.read_Y(train_Y1);


        System.out.println("Fine tuning.................");
        FloatMatrix train_Y = new FloatMatrix(train_Y1);
        train_Y1 = null;
        DBNBatch.finetune(train_X, train_Y, finetune_lr, finetune_epochs);

        float[][] test_Xtmp = new float[2][881];
        ReadCSV r78 = new ReadCSV();
        r78.read_X_test(test_Xtmp);

        FloatMatrix test_X = new FloatMatrix(test_Xtmp);
        test_Xtmp = null;
        FloatMatrix test_Y;
        System.out.println();
        System.out.println("Saving......................\n\n.");
        // test
//        for (int i = 0; i < test_N; i++) {
        DBNBatch.predict();
//        System.out.println("final op");
//        float[][] tmpmatrix = test_Y.dup().toArray2();
//        for (int y = 0; y < tmpmatrix.length; y++) {
//            for (int x = 0; x < tmpmatrix[y].length; x++) {
//                System.out.print("  " + tmpmatrix[y][x]);
//
//            }
//            System.out.println();
//        }
//        }

//        WriteToCSV wc = new WriteToCSV();
//        wc.write(test_Y);
        System.out.println("COMPLETE!!!");
    }

    public static void main(String[] args) throws IOException {
        long start = System.currentTimeMillis();
        test_dbn();
        System.out.println("GPU took: " + (System.currentTimeMillis() - start) / 1000f + "s!");
    }
}